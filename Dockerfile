FROM rust:1.78.0-buster
LABEL authors="ananas"

WORKDIR /usr/src/discord_embed_fix_bot
COPY . .

RUN cargo install --path .

CMD ["discord_embed_fix_bot"]