## Discord emote fixing bot

Lightweight discord bot that listens for messagex with twitter.com/x.com/instagram.com links, removes the broken or incomplete default embed (if present) and replies to the original message without pinging with the original links modified to use the popular embed fixing proxies.

### Configuration

Application only requires a discord bot token, specified either in the `settings.toml` configuration file or passed to the runtime via `APP_token=""` environment variable.
