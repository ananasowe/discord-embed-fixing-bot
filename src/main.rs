mod globals;

use std::collections::HashMap;
use std::env;
use config::Config;

use serenity::async_trait;
use serenity::model::channel::Message;
use serenity::prelude::*;
use log::{debug, error, info};
use serenity::all::{Guild, Ready, ResumedEvent};
use tokio::sync::oneshot::channel;
use regex::Regex;
use globals::DOMAIN_FIXES;
use crate::globals::{DomainRegex, DOMAINS};

mod event;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn guild_create(&self, _: Context, guild: Guild, is_new: Option<bool>) {
        info!("Guild created! new={:?},name={}", is_new.unwrap_or(false), guild.name);
        let channel_names: Vec<String> = guild.channels.values()
            .filter(|guild_channel| guild_channel.is_text_based())
            .map(|guild_channel| guild_channel.name.clone()).collect();
        if (channel_names.is_empty()) {
            error!("Joined a server without a single channel!");
            return
        }
        debug!(
            "Channels present: {:?}",
                channel_names
        );
    }

    async fn ready(&self, _: Context, ready: Ready) {
        info!("Connected as {}", ready.user.name);
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        info!("Resumed");
    }
}

#[tokio::main]
async fn main() {
    env_logger::init();

    let settings = Config::builder()
        // Add in `./settings.toml`
        .add_source(config::File::with_name("./settings.toml").required(false))
        // Add in settings from the environment (with a prefix of APP)
        // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
        .add_source(config::Environment::with_prefix("APP"))
        .build()
        .unwrap();

    debug!(
        "Loaded preferences: {:?}",
        settings
            .clone()
            .try_deserialize::<HashMap<String, String>>()
            .unwrap()
    );

    // Login with a bot token from the environment
    let token = settings.get_string("token").expect("Couldn't resolve the bot token");
    // Set gateway intents, which decides what events the bot will be notified about
    let intents = GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::MESSAGE_CONTENT
        | GatewayIntents::GUILD_MEMBERS
        | GatewayIntents::GUILDS;


    // Create a new instance of the Client, logging in as a bot.
    let mut client =
        Client::builder(&token, intents)
            .event_handler(Handler)
            .event_handler(event::message::Handler)
            .await.expect("Err creating client");

    // Start listening for events by starting a single shard
    if let Err(why) = client.start().await {
        error!("Client error: {why:?}");
    }
}
