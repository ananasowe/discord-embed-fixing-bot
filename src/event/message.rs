use std::time::Duration;
use log::{error, info};
use serenity::all::{EditMessage, Event};
use serenity::async_trait;
use serenity::futures::StreamExt;
use serenity::model::channel::Message;
use serenity::prelude::*;
use crate::globals::{DOMAIN_FIXES, DomainRegex, DOMAINS};

pub(crate) struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, mut msg: Message) {
        if msg.content == "discord_embed_fix_bot" {
            if let Err(why) = msg.reply(&ctx.http, "That's me!").await {
                error!("Error sending message: {why:?}")
            }
            return
        }

        let lock = ctx.data.read().await;
        let links: Vec<(&str, Option<&str>)> = DOMAINS.captures_iter(&msg.content).map(|caps| {
            (caps.get(0).map_or("", |m| m.as_str()),
             caps.name("domain").map(|m| m.as_str()))
        }).collect();
        if (links.is_empty()) {
            return
        }
        let mut response = "That should fix the embeds for you:".to_owned();
        for (link) in links {
            let domain = link.1;
            if (domain.is_none()) {
                continue
            }
            let found_domain = domain.unwrap();
            info!("For message [sender={},content={}] matched the link '{}' in domain {}",
                msg.author.name, msg.content, link.0, found_domain);
            let replacement = DOMAIN_FIXES.get(found_domain).expect("No replacement found for matched domain!");
            let result = link.0.replace(found_domain, replacement);
            response.push('\n');
            response.push_str(&result);
        }
        if let Err(why) = msg.reply(&ctx.http, response).await {
            error!("Could not reply to message: {why:?}")
        }
        let orig_msg_id = msg.id;
        let mut message_updates = serenity::collector::collect(&ctx.shard,
                                                               move |ev| match ev {
            Event::MessageUpdate(x) if x.id == orig_msg_id => Some(()),
            _ => None,
        });
        let _ = tokio::time::timeout(Duration::from_millis(4000), message_updates.next()).await;

        if let Err(why) = msg.edit(&ctx.http, EditMessage::new().suppress_embeds(true)).await{
            error!("Error editing message (embed supression): {why:?}")
        }
        drop(lock);
    }
}