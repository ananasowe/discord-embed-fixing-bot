use lazy_static::lazy_static;
use phf::phf_map;
use regex::Regex;
use serenity::prelude::TypeMapKey;

pub(crate) struct DomainRegex;

impl TypeMapKey for DomainRegex {
    type Value = Regex;
}
pub(crate) const DOMAIN_FIXES: phf::Map<&'static str, &'static str> = phf_map! {
    "twitter" => "i.fxtwitter",
    "x" => "i.fixupx",
    "instagram" => "ddinstagram"
};

lazy_static! {
    pub (crate) static ref DOMAINS: Regex = Regex::new(r"https:\/\/(?<www>www.)?(?<domain>x|twitter|instagram)\.com.*").unwrap();
}